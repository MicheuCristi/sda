package com.sda.fundamentals;

import java.util.Scanner;

public class Exercises {
    public static void main(String[] args) {
        longestSubsequence();
//        gapBetweenLetters();
//
//        getSumOfDigits();
        //simpleCalculatorExercise();
    }



    public static void simpleCalculatorExercise(){
        boolean found = false;
        do {



            Scanner scanner = new Scanner(System.in);
            float firstNumber = scanner.nextFloat();
            String operator = scanner.next().trim();
            float secondNumber = scanner.nextFloat();

            String[] symbolsList = new String[]{"+", "-", "/", "*"};


            for ( int i = 0; i < symbolsList.length; i++){
                // folosim equals pentru a compara valoarea obiectului de tipul String. Daca folosim "==", atunci se vor compara adresele de memorie (care pot fi diferite)
                if ( operator.equals(symbolsList[i])){
                    found = true;
                }
            }
            if (found == false){
                System.out.println("Cannot calculate");
            }else {

                Calculator calculator = new Calculator();
                float result =  calculator.calculate(firstNumber,operator,secondNumber);
                System.out.println(result);
            }
        }
        while (found == false);
    }

    public static void getSumOfDigits(){
        int positiveNumber;
        Scanner scanner = new Scanner(System.in);
        positiveNumber = scanner.nextInt();
        if (positiveNumber < 0){
            System.out.println("The number should be positive");
            return;
        }
        int sum = 0;
        while (positiveNumber > 0){
            int digit = positiveNumber % 10; // pentru a afla ultima cifra
            sum+= digit;
            positiveNumber /= 10;
        }
        System.out.println("sum of digits is = " + sum);



    }

    public static void gapBetweenLetters(){
        Scanner scanner = new Scanner(System.in);
        char letter1 = scanner.next().charAt(0);
        char letter2 = scanner.next().charAt(0);

        int asciiRepresentation1 = letter1;
        int asciiRepresentation2 = letter2;

        int difference = Math.abs(asciiRepresentation1 -asciiRepresentation2);

        if (difference == 0){
            System.out.println("Letters are identical");
        }else {
            System.out.println("There are " + (difference - 1) + " Characters between the letters");
        }

    }


    public static void longestSubsequence(){
        int[] subsequents = new int[10];
        Scanner numbers = new Scanner(System.in);
        System.out.println("Please write 10 numbers");
// iteram cu for pantru a citi de la tastatura 10 numere
        for (int i = 0; i < subsequents.length; i++){
            int number = numbers.nextInt();
            subsequents[i] = number;

        }
// in variabila sum vom retine lungimea subsecventei curente
        int sum = 1;
// aici vom retine cea mai lunga subsecventa numarata pana acum
        int longestSubsequence = 1;
// daca numarul de pe pozitia curenta (subsequents[j] este mai mare decat cel de pe pozitia anterioara subsequents [j-1], atunci vom incrementa sum
        for (int j = 1; j < subsequents.length; j++){
            if (subsequents[j] > subsequents[j-1]){
                sum++;
// daca numarul de pe pozitia curenta este mai mic, inseamna ca subsecventa a fost intrerupta si trebuie sa numaram din nou, dupa ce retinem maximul dintre sum si longest subsequence
            }else{
                longestSubsequence = Math.max(sum , longestSubsequence);
                sum = 1;
            }



        }
// daca ultimul numar din array face parte din cea mai lunga sebsecventa, va trebui sa luam in considerare si ultima valoare a lui sum
        longestSubsequence = Math.max(sum, longestSubsequence);
        System.out.println(longestSubsequence);
    }







}

