package com.sda.fundamentals.exercise11;

import java.io.StringReader;
import java.util.Scanner;

public class Exercise11 {
    public static void main(String[] args) {
        solutionWithVariables();
//        Scanner scanner = new Scanner(System.in);
//        String[] texts = new String[100];
//
//        int index = 0;
//
//        while (true) {
//            String text = scanner.nextLine();
//            if (text.equals("Enough!")) {
//                break;
//            }
//
//            texts[index] = text;
//            index++;
//
//
//        }
//        String longestText = texts[0];
//        for (int i =0; i< index; i++){
//            if (longestText.length() < texts[i].length()) {
//                longestText = texts[i];
//            }
//        }
//        System.out.println("The longest text is: " + longestText);
    }

    public static void solutionWithVariables(){
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String longestText = text;

        while (!text.equals("Enough!")){
            text = scanner.nextLine();

            if (text.equals("Enough!")){
                break;
            }

        }
        if (longestText.equals("Enough!")){
            System.out.println("No text provided");
        }else{
            System.out.println("Longest text: " + longestText);
        }
    }

}



