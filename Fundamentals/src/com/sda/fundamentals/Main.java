package com.sda.fundamentals;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


    // gapBetweenLetters();
    // getSumOfDigits();
    // simpleCalculatorExercise();

        //pentru a apela metoda .nextLine, avem nevoie sa cream un obiect cu new si metoda va fi apelata prin intermediul obiectului
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();

        //metoda statica va fi apelata prin intermediul clasei, nu al obiectului
        Exercises.longestSubsequence();
    }
}

